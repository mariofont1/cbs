from django.db import models

class Account(models.Model):
	nickname_text = models.CharField(max_length=200)
	balance = models.IntegerField(default=0)
	creation_date = models.DateTimeField('date created')

class Customer(models.Model):
    customer = models.ForeignKey(Account, on_delete=models.CASCADE)
    name_text = models.CharField(max_length=200)
    surname_text = models.CharField(max_length=200)
    joined_date = models.DateTimeField('date joined')