from django.contrib import admin

from .models import Customer, Account

admin.site.register(Account)
admin.site.register(Customer)
