# Incluimos la versión de Python
FROM python:3.9

# Copiamos al entorno el archivo de requisitos y lo ejecutamos con pip
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Copiamos el contenido de la app en Django y marcamos ese directorio como el de trabajo actual
COPY . corebanking
WORKDIR /corebanking

# Exponemos el puerto 5000
EXPOSE 5000 

# Abrimos un servidor en el puerto 5000
ENTRYPOINT ["python", "corebanking/manage.py"] 
CMD ["runserver", "0.0.0.0:5000"] 
